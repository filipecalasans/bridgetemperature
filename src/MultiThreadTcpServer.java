import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import javax.sql.ConnectionEventListener;

import org.json.simple.JSONObject;

import net.tinyos.message.Message;
import net.tinyos.message.MessageListener;
import net.tinyos.message.MoteIF;
import net.tinyos.message.SerialPacket;
import net.tinyos.packet.BuildSource;
import net.tinyos.packet.PhoenixSource;
import net.tinyos.util.PrintStreamMessenger;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;



public class MultiThreadTcpServer  implements MessageListener{
	
	PhoenixSource phoenix;
	MoteIF moteIf;
	LinkedList<Socket> clientSockets;
	
	final public static int SERVER_PORT = 60000;
	ServerSocket serverSocket;
	String serialSource;
	
	String dashboardUrl;
	
	private final String USER_AGENT = "Mozilla/5.0";
	
	int count = 0;
	
	public MultiThreadTcpServer(String serialSource, String dasboardAddr) throws Exception  {
		dashboardUrl = dasboardAddr;
		clientSockets = new LinkedList<Socket>();
		phoenix = BuildSource.makePhoenix(serialSource, PrintStreamMessenger.err);
		moteIf = new MoteIF(phoenix);
		SerialPacket msg = new SerialPacket();
		msg.amTypeSet(100);
		moteIf.registerListener(msg, this);
			
		serverSocket = new ServerSocket(SERVER_PORT);
		this.serialSource = serialSource;
		
	}
	
	public void waitConnections() throws Exception {
		
	  System.out.println("Server Listening...");
	  
	  while (true) {
	     Socket sock = serverSocket.accept();
	     System.out.println("New Client Connected" + sock.getInetAddress().toString());
	     
	     clientSockets.addLast(sock);
	     
	     /* TODO: add event Listener to socket disconnection */
	     
	  }
	}
	
	public static void main(String args[]) throws Exception {
		if(args.length == 0) {
			System.out.println("bridge_warm serial@/usr/dev/tty  <dashBoard address>");
			System.out.println("bridge_warm network@<IP>:<PORT> <dashBoard address>");
			System.exit(0);
		}
		
		else if(args.length < 2) {
			if(args[0].compareTo("-h") == 0) {
				System.out.println("bridge_warm serial@/usr/dev/tty <dashBoard address>");
				System.out.println("bridge_warm network@<IP>:<PORT> <dashBoard address>");
				System.exit(0);
			}
		}
		
		if (args.length > 0){
			MultiThreadTcpServer server = new MultiThreadTcpServer(args[0], args[1]);
			server.waitConnections();
		}
	}

	@Override
	public void messageReceived(int id, Message msg) throws NumberFormatException {
		JSONObject json = new JSONObject();
		SerialPacket packet = (SerialPacket)msg;
		
		if(packet.amType() == 100) {
			
			/*  String Format
			 *  
			 *  printf("\n%s %d (Origin: node %d)",
             *  argString[taskInstance],
             *  dataToPrint,
             *  dataOriginId);
             *  
             **/
			String receivedString = new String(packet.dataGet());
			String[] dataString = receivedString.split("\\(");
			String[] splitString = dataString[0].split(": ");
			String type = splitString[0].replaceAll("\n", "");
			String data = splitString[1].replaceAll(" ", "");
			String nodeId = dataString[1];
			nodeId = nodeId.replaceAll("\\)", "");
			String node = nodeId.split("Origin: node")[1];
			node = node.replaceAll(" ", "");
			
			json.put("packetType", "DATA");
			json.put("dataType", type);
			json.put("value", new Integer(data));
			json.put("nodeId", new Integer(node));
			
			try {
				sendGET(2, new Integer(data), count++);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		String dataString = json.toJSONString() + "\n";
		
		if(!json.isEmpty()) {
			for(Socket socket : clientSockets) {
				if(!socket.isClosed()) {
					try {
						socket.getOutputStream().write(dataString.getBytes());
					} catch (IOException e) {
						//e.printStackTrace();
					}
				}
			}
		}
	}
	
	
	private void sendGET(int nodId, int measurement, int count) throws Exception {
		
		String urlParameters =  "?sensor=" + Integer.toString(2) + "&" + 
				"temp=" + measurement + "&" + 
				"counter=" + count;
		
		String url = dashboardUrl + "/newMeasurement/" + urlParameters ; 
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
	}
	
}
